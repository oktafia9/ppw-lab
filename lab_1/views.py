from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Oktafia Sutanti' # TODO Implement this
mhs_name_teman = 'Adelia Utami'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 10, 11) #TODO Implement this, format (Year, Month, Date)
birth_date_teman = date(2000, 1, 21)
npm = 1706023486 # TODO Implement this
npm_teman = 1706984493
tempat_kuliah = 'Fasilkom, Universitas Indonesia'
hobi = 'Menonton film'
hobi_teman = 'Danusan'
deskripsi_diri = '-'
deskripsi_diri_teman = 'anak terakhir dari 3 bersaudara' 

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'name_teman': mhs_name_teman, 'age': calculate_age(birth_date.year), 'age_teman': calculate_age(birth_date.year),'npm': npm, 'npm_teman' : npm_teman,'hobi' : hobi, 'hobi_teman':hobi_teman,'tempat_kuliah' : tempat_kuliah, 'deskripsi_diri' : deskripsi_diri, 'deskripsi_diri_teman':deskripsi_diri_teman}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
